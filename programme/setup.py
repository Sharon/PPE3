from setuptools import setup

setup(
    author="Gregory David",
    author_email="gregory.david@bts-malraux.net",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: End User/Desktop',
        'License :: OSI Approved :: GNU General Public License c3 or later (GPLv3+)'
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Topic :: Multimedia :: Sound/Audio',
        'Topic :: Utilities'],
    description="Démonstration d'un projet avec setuptools",
    entry_points={
        'console_script' : [
            'programme-executable = programme:run'
        ]
    },
    install_requires=["lxml","psycopg2"],
    keywords='radio playlist generator',
    licence="GPLv3",
    long_description="Aucune inspiration pour rédiger cette longue vraiment longue description",
    name="programme",
    packages=["programme","programme.ressources"],
    python_requires='>3.2, <4',
    url="http://bts.bts-malraux72.net/~g.david",
    version="0.4.2"
)
