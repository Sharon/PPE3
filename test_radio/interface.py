    

        # -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 11:04:20 2017

    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.
"""

import logging
import logging.handlers
import os

# On confiure les logs
user = logging.getLogger('__name__')
user.setLevel(logging.DEBUG)


if not os.path.exists('logs'): os.mkdir('logs') # Si le dossier de log n'existe pas alors on le creer
log_file_handler = logging.handlers.TimedRotatingFileHandler('logs/zealeov.log', when='M', interval=2)
log_file_handler.setFormatter( logging.Formatter('%(asctime)s [%(levelname)s](%(name)s:%(funcName)s:%(lineno)d): %(message)s'))
log_file_handler.setLevel(logging.DEBUG)
user.addHandler(log_file_handler)

console_handler = logging.StreamHandler() # sys.stderr
console_handler.setLevel(logging.CRITICAL) # set later by set_log_level_from_verbose() in interactive sessions
console_handler.setFormatter( logging.Formatter('[%(levelname)s](%(name)s): %(message)s') )
user.addHandler(console_handler)




