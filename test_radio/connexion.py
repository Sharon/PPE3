
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 09:53:06 2017

@author: PANTHIER Sharon

    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

"""

# Connexion a la base de données radio-libre avec Postgre
import psycopg2
import interface


"""Test de connexion a la base de données """

rows = []

try:
    #connextion a la base de donnée
    connect_str = "dbname='radio_libre' user='s.panthier' host='postgresql.bts-malraux72.net' password='P@ssword'"
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
    interface.logging.info('La connexion a la bdd a réussi')
except Exception as e:
    print('La connexion a la BDD a échoué')
    interface.logging.error(e)
try:
    #TODO : Faire des requêtes préparées 
    sql = """SELECT * from morceaux """
    cursor.execute(sql)
    rows = cursor.fetchall()
    interface.logging.info('les morceaux ont bien été récupérés')
    cursor.close()
    conn.close()
except Exception as e:
    print('Erreur lors de la requete : %s ' % e)
    interface.logging.error(e)
  


