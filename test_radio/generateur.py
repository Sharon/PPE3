         

# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 09:53:06 2017

    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to redistribute it
    under certain conditions.

""" 
# 
import argparse
import interface
import fichiers
import whereGen

#TODO : mettre le nom de la playliste et sa durée en obligatoire
ma_cli = argparse.ArgumentParser("Générateur de playliste : Zealeov")
ma_cli.add_argument('-V', '--version', action="version", version="%(prog)s 1.1")
#ma_cli.add_argument('file', help="nom de la playliste", metavar='FILE')
ma_cli.add_argument('-d','--debug', help='Enclenche le mode debug de la console', action='store_true', default=False)
ma_cli.add_argument('-v','--verbose', help='Augmente la verbosité du langage', action="count", default=0)
#ma_cli.add_argument('duree', help='Durée total de la playlist souhiatée', type = str)
ma_cli.add_argument('--genre',help='Tri par genre de morceaux dans la playlist', nargs=2, action='append')
ma_cli.add_argument('--album',help='Tri par nom d\'album de la playlist', nargs=2, action='append')
ma_cli.add_argument('--sortie',help='Type de sortie de la playliste', type = str)
ma_cli.add_argument('--titre',help='Tri par titre de morceaux dans la playlist', nargs=2, action='append')
    
# Affichage plus ou moins verbeux en fonction du nombre de paramètre -v
def set_log_level_from_verbose(args):
    if not args.verbose:
        interface.console_handler.setLevel('ERROR')
    elif args.verbose == 1:
        interface.console_handler.setLevel('WARNING')
    elif args.verbose == 2:
        interface.console_handler.setLevel('INFO')
    elif args.verbose >= 3:
        interface.console_handler.setLevel('DEBUG')
    else:
        interface.user.critical("UNEXPLAINED NEGATIVE COUNT!")
   
def recherche_args(args):
    for critere in ['genre','album','titre']:
        argument = getattr(args, critere) 
        print(argument)
        whereGen.whereGen(argument,critere)
    
        
if __name__ == '__main__':
    args = ma_cli.parse_args()
    set_log_level_from_verbose(args)
    recherche_args(args)
       
    

   


    
    
  
